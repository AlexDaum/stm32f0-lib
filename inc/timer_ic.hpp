#pragma once

#include "dma.h"
#include "stm.h"
#include "timer.h"

namespace stlib {

enum class ICFilter : u8 {
	NONE = 0,
	KER_CK_2 = 1,
	KER_CK_4 = 2,
	KER_CK_8 = 3,
	DTS_D2_6 = 4,
	DTS_D2_8 = 5,
	DTS_D4_6 = 6,
	DTS_D4_8 = 7,
	DTS_D8_6 = 8,
	DTS_D8_8 = 9,
	DTS_D16_5 = 10,
	DTS_D16_6 = 11,
	DTS_D16_8 = 12,
	DTS_D32_5 = 13,
	DTS_D32_6 = 14,
	DTS_D32_8 = 15
};

enum class ICPSC : u8 { NONE = 0, PRE2 = 1, PRE4 = 2, PRE8 = 3 };
enum class ICSEL : u8 { OWN = 1, ALT = 2, TRC = 3 };
enum class ICEDGE : u8 { RISING = 0, FALLING = 1, BOTH = 3 };

template <typename T> class TimerICConf : public TimerCCConf {
  public:
	constexpr TimerICConf(u8 channel, ICEDGE edge,
						  ICFilter filter = ICFilter::NONE,
						  ICPSC psc = ICPSC::NONE, ICSEL sel = ICSEL::OWN)
		: TimerCCConf(channel), config(createConfig(filter, psc, sel)),
		  edge(edge) {}

  protected:
	virtual void apply(TIM_TypeDef *timer) const override {
		if ((m_channel == 1) || (m_channel == 2)) {
			setReg<u32>(&timer->CCMR1, 0xFF << 8 * (m_channel - 1),
						config << (m_channel - 1UL));
		} else {
			setReg<u32>(&timer->CCMR2, 0xFF << 8 * (m_channel - 3),
						config << (m_channel - 3UL));
		}
		const u32 ccerMask = (TIM_CCER_CC1P_Msk | TIM_CCER_CC1NP_Msk)
							 << 4 * (m_channel - 1);
		setReg<u32>(&timer->CCER, ccerMask, static_cast<u32>(edge));
	}
	virtual void configDMA(DMAChannel &dma, TIM_TypeDef *timer, uintptr_t maddr,
						   u16 cnt) const override {
		DMAChannelConfig conf;
		conf.setCirc(false)
			.setDir(DMADir::PeriphToMem)
			.setMSize(getMemSize())
			.setPSize(DMAMemSize::size32bit)
			.setMem2Mem(false)
			.setMinc(true)
			.setPinc(false);
		dma.configure(conf);
		uintptr_t paddr;
		switch (m_channel) {
		case 1:
			paddr = reinterpret_cast<uintptr_t>(&timer->CCR1);
			break;
		case 2:
			paddr = reinterpret_cast<uintptr_t>(&timer->CCR2);
			break;
		case 3:
			paddr = reinterpret_cast<uintptr_t>(&timer->CCR3);
			break;
		case 4:
			paddr = reinterpret_cast<uintptr_t>(&timer->CCR4);
			break;
		default:
			paddr = 0;
		}
		dma.configureTransfer(maddr, paddr, cnt);
		timer->DIER |= TIM_DIER_CC1DE << (m_channel - 1);
	}

  private:
	const u8 config;
	bool dma = false;
	ICEDGE edge;

	static constexpr u8 createConfig(ICFilter filter, ICPSC psc, ICSEL sel) {
		u8 temp = 0;
		temp |= static_cast<u8>(filter) << TIM_CCMR1_IC1F_Pos;
		temp |= static_cast<u8>(psc) << TIM_CCMR1_IC1PSC_Pos;
		temp |= static_cast<u8>(sel) << TIM_CCMR1_CC1S_Pos;
		return temp;
	}
	static constexpr DMAMemSize getMemSize();
};

template <> inline DMAMemSize TimerICConf<u16>::getMemSize() {
	return DMAMemSize::size16bit;
}
template <> inline DMAMemSize TimerICConf<u32>::getMemSize() {
	return DMAMemSize::size32bit;
}

} // namespace stlib
