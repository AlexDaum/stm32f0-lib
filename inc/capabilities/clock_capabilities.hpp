#pragma once

#include <cstdint>
namespace stlib::capabilities::clock {

/**
 * Clock Capabilities for STM32F0
 *
 * @author Alexander Daum
 */

constexpr uint32_t max_clock = 48000000;

/*
 * Type of Main PLL in the Device
 * Normal: The PLL can output integer multiples of the input clock
 * Fractional: The PLL Output clock is a fraction of the VCO clock and the VCO
 * clock can be (significantly) higher than the maximum system clock
 */
enum class PLLType { Normal, Fractional };

constexpr PLLType pll_type = PLLType::Normal;

} // namespace stlib::capabilities
