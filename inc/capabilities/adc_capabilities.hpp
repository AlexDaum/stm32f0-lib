#pragma once

/**
 * ADC Capabilities for STM32F0
 *
 * @author Alexander Daum
 */

namespace stlib::capabilities::adc {

/*
 * ADC Sequence type.
 * Simple: The ADC Channels are sampled in Order of the channel number
 * (ascending or descending) Ordered: The ADC Channels are sampled in a User
 * defined order
 */
enum class ADCSequenceType { Simple, Ordered };

constexpr ADCSequenceType sequence_type = ADCSequenceType::Simple;

/*
 * ADC Sampling time type
 * Shared: All ADC Channels use the same sampling time
 * ChannelSpecific: Each ADC Channel can have a different sampling time
 */
enum class ADCSamplingTime { Shared, ChannelSpecific };

constexpr ADCSamplingTime sampling_type = ADCSamplingTime::Shared;

/*
 * Shows if the ADC supports the bulb mode, where it starts sampling directly
 * after the conversion is finished
 */
constexpr bool bulb_mode_supported = false;

} // namespace stlib::capabilities::adc
