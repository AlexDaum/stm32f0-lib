#pragma once
/*
 * timer.h
 *
 *  Created on: 15.11.2018
 *      Author: alex
 */

#include "device/timer_device.hpp"
#include "dma.h"
#include "haldefs.h"
#include "stm.h"
#include "utils.h"
#include <algorithm>
#include <array>
#include <clocks.h>
#include <timebase.h>
#include <timer_traits.h>

namespace stlib {

class TimerCCConf {
	template <typename T> friend class Timer;

  public:
	constexpr TimerCCConf(u8 channel) : m_channel(channel) {}

  protected:
	virtual void apply(TIM_TypeDef *timer) const = 0;
	virtual void configDMA(DMAChannel &dma, TIM_TypeDef *timer, uintptr_t maddr,
						   u16 cnt) const = 0;
	const u8 m_channel;
};

enum class TrigoConf : u8 { none = 0, enable = 1, update = 2 };

template <typename T> class Timer {
  public:
	constexpr Timer(TimerDevice iTimer) : m_iTimer(static_cast<u8>(iTimer)) {}

	void init() { TimerStaticHelper::timer_clocks[m_iTimer].set(); }

	void setPresc(u16 presc);

	void setARR(T arr);

	void setApproxFreq(u32 targetFreq);

	void setApproxPeriod(u32 period, timebase_t timebase = timebase_t::MILLIS);

	void configChannel(const TimerCCConf &config) { config.apply(getTim()); }

	void configChannelDMA(const TimerCCConf &config, DMAChannel &ch,
						  uintptr_t maddr, u16 cnt) {
		config.apply(getTim());
		config.configDMA(ch, getTim(), maddr, cnt);
	}

	void start() { getTim()->CR1 |= TIM_CR1_CEN; }
	void stop() { getTim()->CR1 &= ~TIM_CR1_CEN; }

	T getCount() const { return getTim()->CNT; }
	void zeroCount() {
		auto tim = getTim();
		tim->CNT = 0;
		tim->SR = ~TIM_SR_UIF;
	}

	void configTrigo(TrigoConf conf) {
		modifyReg(&getTim()->CR2, TIM_CR2_MMS,
				  static_cast<u32>(conf) << TIM_CR2_MMS_Pos);
	}

  private:
	u8 m_iTimer;
	TIM_TypeDef *getTim() { return TimerStaticHelper::timer_map[m_iTimer]; }
	TIM_TypeDef const *getTim() const {
		return TimerStaticHelper::timer_map[m_iTimer];
	}
};

namespace timer_impl {
template <TimerDevice d>
using Timer_t = Timer<typename timer_traits<d>::timerDataType>;
};

template <TimerDevice d> constexpr timer_impl::Timer_t<d> getTimer() {
	return timer_impl::Timer_t<d>(d);
}

/*
 * ==== Implementation ====
 */

template <typename T> void Timer<T>::setPresc(u16 presc) {
	auto tim = getTim();
	tim->PSC = presc;
}

template <typename T> void Timer<T>::setARR(T arr) {
	auto tim = getTim();
	tim->ARR = arr;
}

} // namespace stlib
