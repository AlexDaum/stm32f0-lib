#pragma once

#include "haldefs.h"
#include "stm.h"

namespace stlib {

enum class DMA_DEVICE { dma1 = 0, dma2 = 1 };

class DMA {
  public:
	constexpr DMA(DMA_DEVICE iDMA) : m_iDMA(static_cast<u8>(iDMA)) {}
	void init();

  private:
	u8 m_iDMA;
};

enum class DMAMemSize : u8 { size8bit = 0, size16bit = 1, size32bit = 2 };

enum class DMADir : bool { PeriphToMem = false, MemToPeriph = true };

class DMAChannel;

struct DMAChannelConfig {
	friend class DMAChannel;

  private:
	u16 config = 0;

  public:
	DMAChannelConfig() = default;

	DMAChannelConfig &setMem2Mem(bool mem2mem) {
		if (mem2mem)
			config |= DMA_CCR_MEM2MEM;
		else
			config &= ~DMA_CCR_MEM2MEM;
		return *this;
	}
	DMAChannelConfig &setMSize(DMAMemSize msize) {
		config &= ~DMA_CCR_MSIZE;
		config |= static_cast<u16>(msize) << DMA_CCR_MSIZE_Pos;
		return *this;
	}
	DMAChannelConfig &setPSize(DMAMemSize psize) {
		config &= ~DMA_CCR_PSIZE;
		config |= static_cast<u16>(psize) << DMA_CCR_PSIZE_Pos;
		return *this;
	}
	DMAChannelConfig &setMinc(bool minc) {
		if (minc)
			config |= DMA_CCR_MINC;
		else
			config &= ~DMA_CCR_MINC;
		return *this;
	}
	DMAChannelConfig &setPinc(bool pinc) {
		if (pinc)
			config |= DMA_CCR_PINC;
		else
			config &= ~DMA_CCR_PINC;
		return *this;
	}
	DMAChannelConfig &setCirc(bool circ) {
		if (circ)
			config |= DMA_CCR_CIRC;
		else
			config &= ~DMA_CCR_CIRC;
		return *this;
	}
	DMAChannelConfig &setDir(DMADir dir) {
		if (static_cast<bool>(dir))
			config |= DMA_CCR_DIR;
		else
			config &= ~DMA_CCR_DIR;
		return *this;
	}
};

class DMAChannel {
  public:
	constexpr DMAChannel(const DMA_DEVICE iDMA, u8 iChannel)
		: m_iDMA(static_cast<u8>(iDMA)), m_iChannel(iChannel) {}

	void configure(DMAChannelConfig config);
	void configureTransfer(uintptr_t source, uintptr_t dest, size_t count);
	void enable();
	void disable();
	u8 getMuxChannelNum() const { return m_iDMA * 8 + (m_iChannel - 1); }

  private:
	const u8 m_iDMA, m_iChannel;
};

} // namespace stlib
