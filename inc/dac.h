#pragma once

#include "haldefs.h"

#include "stm.h"

//#define DAC2CH

namespace stlib {
enum class DAC_DEVICE : u8 { dac1 = 0, dac3 = 1 };
enum class DAC_MODE : u8 { BufferPin = 0, NoBufferPin = 1 };
class dac {
  public:
	constexpr dac(DAC_DEVICE iDAC) : m_iDAC(static_cast<u8>(iDAC)) {}

	void init();
	void enable_channel1();
	void set_ch1_data(u16 data);
	void set_ch1_mode(DAC_MODE mode);
#ifdef DAC2CH
	void enable_channel2();
	void set_ch2_data(u16 data);
	void set_ch2_mode(DAC_MODE mode);

	void enable_dma_ch2();
	void select_trigger_ch2(u16 trigger);
#endif

	void enable_dma_ch1();
	void select_trigger_ch1(u16 trigger);

  protected:
	DAC_TypeDef *getDAC() const;
	u8 getiDAC() const { return m_iDAC; }

  private:
	u8 m_iDAC;
};

} // namespace stlib
