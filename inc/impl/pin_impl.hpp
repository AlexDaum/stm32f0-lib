#pragma once

#include "clocks.h"
#include "pin.h"
#include "utils.h"
#include <array>

using namespace stlib;

constexpr std::array<clockEnableBit, 5> portEnableBits{
	clockEnableBit{CLOCK_REG_BASE(AHBENR), RCC_AHBENR_GPIOAEN},
	clockEnableBit{CLOCK_REG_BASE(AHBENR), RCC_AHBENR_GPIOBEN},
	clockEnableBit{CLOCK_REG_BASE(AHBENR), RCC_AHBENR_GPIOCEN},
	clockEnableBit{CLOCK_REG_BASE(AHBENR), RCC_AHBENR_GPIODEN},
	clockEnableBit{CLOCK_REG_BASE(AHBENR), RCC_AHBENR_GPIOFEN},
};

constexpr std::array<uintptr_t, 5> gpioBase{GPIOA_BASE, GPIOB_BASE, GPIOC_BASE,
											GPIOD_BASE, GPIOF_BASE};

void inline stlib::Port::enableClock() const { portEnableBits[m_iPort].set(); }

GPIO_TypeDef inline *stlib::Port::getReg() const {
	return reinterpret_cast<GPIO_TypeDef *>(gpioBase[m_iPort]);
}

const Pin inline &stlib::Pin::set_mode(PinMode mode) const {
	setRegField<u32, 2>(&m_port.getReg()->MODER, m_iPin,
						static_cast<u32>(mode));
	return *this;
}

const Pin inline &stlib::Pin::set_pulldown(PinPUPD pupd) const {
	setRegField<u32, 2>(&m_port.getReg()->PUPDR, m_iPin,
						static_cast<u32>(pupd));
	return *this;
}

const Pin inline &stlib::Pin::set_otype(PinOType type) const {
	setRegField<u32, 1>(&m_port.getReg()->PUPDR, m_iPin,
						static_cast<u32>(type));
	return *this;
}

const Pin inline &stlib::Pin::setSpeed(PinSpeed speed) const {
	setRegField<u32, 2>(&m_port.getReg()->PUPDR, m_iPin,
						static_cast<u32>(speed));
	return *this;
}

void inline stlib::Pin::set(bool state) const {
	if (state)
		set();
	else
		reset();
}

const inline Pin &stlib::Pin::setAF(u8 af) const {
	auto reg = m_port.getReg();
	const size_t iAFR = m_iPin / 8;
	const int j = (m_iPin % 8) * 4;
	setReg<u32>(&reg->AFR[iAFR], GPIO_AFRL_AFSEL0 << j, af << j);
	return *this;
}
/*
 * Pin and Port Definitions
 */
namespace stlib {
constexpr Port PortA(0);
constexpr Port PortB(1);
constexpr Port PortC(2);
constexpr Port PortD(3);
constexpr Port PortF(4);

constexpr Pin PA0(PortA, 0);
constexpr Pin PA1(PortA, 1);
constexpr Pin PA2(PortA, 2);
constexpr Pin PA3(PortA, 3);
constexpr Pin PA4(PortA, 4);
constexpr Pin PA5(PortA, 5);
constexpr Pin PA6(PortA, 6);
constexpr Pin PA7(PortA, 7);
constexpr Pin PA8(PortA, 8);
constexpr Pin PA9(PortA, 9);
constexpr Pin PA10(PortA, 10);
constexpr Pin PA11(PortA, 11);
constexpr Pin PA12(PortA, 12);
constexpr Pin PA13(PortA, 13);
constexpr Pin PA14(PortA, 14);
constexpr Pin PA15(PortA, 15);

constexpr Pin PB0(PortB, 0);
constexpr Pin PB1(PortB, 1);
constexpr Pin PB2(PortB, 2);
constexpr Pin PB3(PortB, 3);
constexpr Pin PB4(PortB, 4);
constexpr Pin PB5(PortB, 5);
constexpr Pin PB6(PortB, 6);
constexpr Pin PB7(PortB, 7);
constexpr Pin PB8(PortB, 8);
constexpr Pin PB9(PortB, 9);
constexpr Pin PB10(PortB, 10);
constexpr Pin PB11(PortB, 11);
constexpr Pin PB12(PortB, 12);
constexpr Pin PB13(PortB, 13);
constexpr Pin PB14(PortB, 14);
constexpr Pin PB15(PortB, 15);

constexpr Pin PC0(PortC, 0);
constexpr Pin PC1(PortC, 1);
constexpr Pin PC2(PortC, 2);
constexpr Pin PC3(PortC, 3);
constexpr Pin PC4(PortC, 4);
constexpr Pin PC5(PortC, 5);
constexpr Pin PC6(PortC, 6);
constexpr Pin PC7(PortC, 7);
constexpr Pin PC8(PortC, 8);
constexpr Pin PC9(PortC, 9);
constexpr Pin PC10(PortC, 10);
constexpr Pin PC11(PortC, 11);
constexpr Pin PC12(PortC, 12);
constexpr Pin PC13(PortC, 13);
constexpr Pin PC14(PortC, 14);
constexpr Pin PC15(PortC, 15);

constexpr Pin PD0(PortD, 0);
constexpr Pin PD1(PortD, 1);
constexpr Pin PD2(PortD, 2);
constexpr Pin PD3(PortD, 3);
constexpr Pin PD4(PortD, 4);
constexpr Pin PD5(PortD, 5);
constexpr Pin PD6(PortD, 6);
constexpr Pin PD7(PortD, 7);
constexpr Pin PD8(PortD, 8);
constexpr Pin PD9(PortD, 9);
constexpr Pin PD10(PortD, 10);
constexpr Pin PD11(PortD, 11);
constexpr Pin PD12(PortD, 12);
constexpr Pin PD13(PortD, 13);
constexpr Pin PD14(PortD, 14);
constexpr Pin PD15(PortD, 15);

constexpr Pin PF0(PortF, 0);
constexpr Pin PF1(PortF, 1);

} // namespace stlib
