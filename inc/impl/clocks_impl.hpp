#pragma once

namespace stlib::clock {

template <clockDomain domain>
inline void
ClockChangeHandler::registerChangeHandler(clockChangeListener<domain> *chl) {
	if constexpr (domain == clockDomain::CPU) {
		if (cpuSpeedRoot != nullptr)
			cpuSpeedRoot->insertBefore(chl);
		cpuSpeedRoot = chl;
	} else if constexpr (domain == clockDomain::AHB) {
		if (ahbSpeedRoot != nullptr)
			ahbSpeedRoot->insertBefore(chl);
		ahbSpeedRoot = chl;

	} else if constexpr (domain == clockDomain::APB) {
		if (apbSpeedRoot != nullptr)
			apbSpeedRoot->insertBefore(chl);
		apbSpeedRoot = chl;

	} else {
		static_assert(domain == clockDomain::CPU ||
						  domain == clockDomain::AHB ||
						  domain == clockDomain::APB,
					  "ERROR, unknown clock domain");
	}
}

inline void ClockChangeHandler::changeClock(uint32_t cpuSpeed, PPRE ppre,
											HPRE hpre) const {
	cpuSpeedRoot->forEachAfter<cpuLis_t>(
		[cpuSpeed](auto ptr) { ptr->onClockChanged(cpuSpeed); });

	u32 ahbSpeed = cpuSpeed / hpreToInt(hpre);
	ahbSpeedRoot->forEachAfter<ahbLis_t>(
		[ahbSpeed](auto ptr) { ptr->onClockChanged(ahbSpeed); });

	u32 apbSpeed = ahbSpeed / ppreToInt(ppre);
	apbSpeedRoot->forEachAfter<apbLis_t>(
		[apbSpeed](auto ptr) { ptr->onClockChanged(apbSpeed); });
}

template <clockDomain domain>
inline void clockChangeListener<domain>::attachHandler() {
	ClockChangeHandler::getInstance().registerChangeHandler(this);
}

} // namespace stlib::clock
