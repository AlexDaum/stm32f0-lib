/*
 * systick.h
 *
 *  Created on: 15.11.2018
 *      Author: alex
 */

#ifndef SYSTICK_H_
#define SYSTICK_H_

#include "clocks.h"
#include "containers/static_linked_list.hpp"
#include <algorithm>
#include <iterator>
#include <stdint-gcc.h>

extern "C" uint32_t SystemCoreClock;

namespace stlib {

class Ticker;

class Systick : clock::clockChangeListener<clock::clockDomain::CPU> {
	friend class Ticker;

  public:
	Systick(const Systick &) = delete;
	void init() { attachHandler(); }
	void irq();
	static Systick &getST() { return stick; }

  protected:
	void onClockChanged(u32 speed) override;

  private:
	Systick() = default;
	Ticker *root;

	static Systick stick;

	void registerTicker(Ticker *t);
};

class Ticker : public StaticLLEntry {
	friend class Systick;

  public:
	Ticker() { Systick::getST().registerTicker(this); }

  protected:
	virtual void tick() = 0;

  private:
};

class SysTimer : public Ticker {
  public:
	virtual void tick() override;

	void wait(uint32_t delay);

  private:
	volatile uint32_t time = 0;
};

class SysCounter : public Ticker {
  public:
	virtual void tick() override;
	void reset();
	uint32_t get();

  private:
	volatile uint32_t time = 0;
};

inline void Systick::registerTicker(Ticker *t) {
	if (root != nullptr)
		root->insertAfter(t);
	else
		root = t;
}

} // namespace stlib
#endif /* SYSTICK_H_ */
