#pragma once

#include "haldefs.h"
#include "stm.h"
#include <algorithm>
#include <containers/static_linked_list.hpp>

/**
 * Clock Configuration
 */
namespace stlib::clock {

constexpr uint32_t HSI_clock = 8000000;

/*
 * Should be changed depending on the external oscillator/crystal used
 */
constexpr uint32_t HSE_clock = 8000000;

enum class ClockSource : u8 { HSI, HSE, PLL, HSI48 };

enum class PLLSource : u8 {
	HSIdiv2 = 0,
	HSIprediv = 1,
	HSEprediv = 2,
	HSI48prediv = 3
};

enum class PPRE : u8 {
	NONE = 0,
	DIV2 = 0b100,
	DIV4 = 0b101,
	DIV8 = 0b110,
	DIV16 = 0b111
};

constexpr u32 ppreToInt(PPRE ppre) {
	u32 ret = 0;
	switch (ppre) {
	case PPRE::NONE:
		ret = 1;
		break;
	case PPRE::DIV2:
		ret = 2;
		break;
	case PPRE::DIV4:
		ret = 4;
		break;
	case PPRE::DIV8:
		ret = 8;
		break;
	case PPRE::DIV16:
		ret = 16;
		break;
	}
	return ret;
}

enum class HPRE : u8 {
	NONE = 0,
	DIV2 = 0b1000,
	DIV4 = 0b1001,
	DIV8 = 0b1010,
	DIV16 = 0b1011,
	DIV64 = 0b1100,
	DIV128 = 0b1101,
	DIV256 = 0b1110,
	DIV512 = 0b1111
};

constexpr u32 hpreToInt(HPRE hpre) {
	u32 ret = 0;
	switch (hpre) {
	case HPRE::NONE:
		ret = 1;
		break;
	case HPRE::DIV2:
		ret = 2;
		break;
	case HPRE::DIV4:
		ret = 4;
		break;
	case HPRE::DIV8:
		ret = 8;
		break;
	case HPRE::DIV16:
		ret = 16;
		break;
	case HPRE::DIV64:
		ret = 64;
		break;
	case HPRE::DIV128:
		ret = 128;
		break;
	case HPRE::DIV256:
		ret = 256;
		break;
	case HPRE::DIV512:
		ret = 512;
		break;
	}
	return ret;
}

enum class clockDomain { CPU, AHB, APB };

template <clockDomain domain>
struct clockChangeListener : public StaticLLEntry {
	friend class ClockChangeHandler;
	void attachHandler();

  protected:
	virtual void onClockChanged(uint32_t newClockSpeed) = 0;
};

namespace {

constexpr uint32_t getPLLSourceSpeed(PLLSource src, u8 prediv) {
	switch (src) {
	case PLLSource::HSIdiv2:
		return HSI_clock / 2u;
	case PLLSource::HSIprediv:
		return HSI_clock / prediv;
	case PLLSource::HSEprediv:
		return HSE_clock / prediv;
	case PLLSource::HSI48prediv:
		return 48000000UL / prediv;
	default:
		return HSI_clock / 2;
	}
}
} // namespace

class ClockChangeHandler {
	using cpuLis_t = clockChangeListener<clockDomain::CPU>;
	using ahbLis_t = clockChangeListener<clockDomain::AHB>;
	using apbLis_t = clockChangeListener<clockDomain::APB>;

  public:
	void changeClock(uint32_t cpuSpeed, PPRE ppre, HPRE hpre) const;

	template <clockDomain domain>
	void registerChangeHandler(clockChangeListener<domain> *chl);

	static ClockChangeHandler &getInstance() { return instance; }

  private:
	cpuLis_t *cpuSpeedRoot;
	ahbLis_t *ahbSpeedRoot;
	apbLis_t *apbSpeedRoot;
	static ClockChangeHandler instance;
};

inline void disableHSI() { RCC->CR = RCC->CR & ~RCC_CR_HSION; }
inline void disableHSE() { RCC->CR = RCC->CR & ~RCC_CR_HSEON; }
inline void disablePLL() { RCC->CR = RCC->CR & ~RCC_CR_PLLON; }
void enableHSI();
void enableHSE();

struct clockConfig {
	constexpr clockConfig(ClockSource src = ClockSource::HSI,
						  PLLSource pllsrc = PLLSource::HSIdiv2,
						  u8 pll_mul = 12, u8 pll_presc = 2,
						  PPRE ppre = PPRE::NONE, HPRE hpre = HPRE::NONE)
		: m_src(src), m_pllsrc(pllsrc), m_pll_mul(pll_mul),
		  m_pll_presc(pll_presc), m_ppre(ppre), m_hpre(hpre) {}

	void apply() const {
		u32 cpuSpeed = getCPUclockSpeed();
		SystemCoreClock = cpuSpeed;

		flashWaitStateCfg(cpuSpeed);

		switch (m_src) {
		case ClockSource::HSI:
			enableHSI();
			switchClock(RCC_CFGR_SW_HSI);
			break;
		case ClockSource::HSE:
			enableHSE();
			switchClock(RCC_CFGR_SW_HSE);
			break;
		case ClockSource::PLL:
			confPLL();
			switchClock(RCC_CFGR_SW_PLL);
			break;
		default:
			enableHSI();
			switchClock(RCC_CFGR_SW_HSI);
		}
		ClockChangeHandler::getInstance().changeClock(cpuSpeed, m_ppre, m_hpre);
	}

	constexpr u32 getCPUclockSpeed() const {
		switch (m_src) {
		case ClockSource::HSI:
			return HSI_clock;
		case ClockSource::HSE:
			return HSE_clock;
		case ClockSource::PLL:
			return getPLLSourceSpeed(m_pllsrc, m_pll_presc) * m_pll_mul;
		case ClockSource::HSI48:
			return 48000000;
		}
		return HSI_clock;
	}

  private:
	void confPLL() const;
	static void switchClock(u32 sw);

	static void flashWaitStateCfg(u32 clock) {
		constexpr u32 _24MHz = 24000000;
		if (clock < _24MHz) {
			FLASH->ACR &= ~FLASH_ACR_LATENCY & ~FLASH_ACR_PRFTBE;
		} else {
			FLASH->ACR |= (1 << FLASH_ACR_LATENCY_Pos) | FLASH_ACR_PRFTBE;
		}
	}

  public:
	ClockSource m_src;

	PLLSource m_pllsrc;
	u8 m_pll_mul;
	u8 m_pll_presc;
	PPRE m_ppre;
	HPRE m_hpre;
};

/**
 *
 */
constexpr clockConfig createPLLConfig(uint32_t targetFreq,
									  PLLSource source = PLLSource::HSIdiv2,
									  u8 prediv = 2) {
	uint32_t sourceFreq = getPLLSourceSpeed(source, prediv);
	int mul = targetFreq / sourceFreq;
	if (mul > 16)
		mul = 16;
	if (mul < 2)
		mul = 2;
	return clockConfig(ClockSource::PLL, source, mul, prediv);
}
} // namespace stlib::clock
/*
 * Clock enabling
 */
#define CLOCK_REG_BASE(name) (RCC_BASE + offsetof(RCC_TypeDef, name))
namespace stlib {

struct clockEnableBit {
	const uintptr_t reg_base;
	const u32 mask;

	constexpr clockEnableBit(uintptr_t p_reg_base, u32 p_mask)
		: reg_base(p_reg_base), mask(p_mask) {}

	void set() const { *reg() |= mask; }

	void clear() const { *reg() &= ~mask; }

  private:
	volatile u32 *reg() const {
		return reinterpret_cast<volatile u32 *>(reg_base);
	}
};

constexpr clockEnableBit sysCfgEn{CLOCK_REG_BASE(APB2ENR),
								  RCC_APB2ENR_SYSCFGEN};

} // namespace stlib

#include "impl/clocks_impl.hpp"
