#pragma once
#include "Ringbuffer.hpp"
#include "uart.hpp"
#include <functional>

namespace stlib {

template <size_t RingBufSize> class RingBufferUart : public Uart {
  public:
	explicit RingBufferUart(UartDevice iUart) : Uart(iUart) {
		auto fn = &stlib::RingBufferUart<RingBufSize>::irq;
		auto mf = createMemberFun(fn, this);
		attachUartInterrupt(static_cast<size_t>(iUart), mf);
	}

	void configure(const UartConfig &cfg) override {
		Uart::configure(cfg);
		auto uart = getUart();
		uart->CR1 |= USART_CR1_RXNEIE;
	}

	void send(u8 d) override {
		while (sendBuffer.isFull())
			;
		sendBuffer.put(d);
		getUart()->CR1 |= USART_CR1_TXEIE;
	}

	bool dataReady() override { return !receiveBuffer.isEmpty(); }

	u8 getData() { return receiveBuffer.get(); }

  protected:
	virtual void overrun() {}

  private:
	Ringbuffer<u8, RingBufSize> sendBuffer;
	Ringbuffer<u8, RingBufSize> receiveBuffer;
	void irq() {
		auto uart = getUart();
		if (uart->ISR & USART_ISR_RXNE) {
			receiveBuffer.put(uart->RDR);
		}
		if (uart->ISR & USART_ISR_TXE) {
			if (sendBuffer.isEmpty())
				uart->CR1 &= ~USART_CR1_TXEIE;
			else
				uart->TDR = sendBuffer.get();
		}
		if (uart->ISR & USART_ISR_ORE) {
			overrun();
			uart->ICR = USART_ICR_ORECF;
		}
	}
};

} // namespace stlib
