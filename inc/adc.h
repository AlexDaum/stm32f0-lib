/*
 * adc.h
 *
 *  Created on: 31.03.2020
 *      Author: alex
 */

#ifndef INC_LIB_ADC_H_
#define INC_LIB_ADC_H_

#include "haldefs.h"
#include <array>
#include <functional>

namespace stlib {

enum class AdcSampleTime : u8 {
	Smpl1_5 = 0,
	Smpl7_5 = 1,
	Smpl13_5 = 2,
	Smpl28_5 = 3,
	Smpl41_5 = 4,
	Smpl55_5 = 5,
	Smpl71_5 = 6,
	Smpl239_5 = 7
};

enum class ADC_DEVICE : u8 { adc1 = 0, adc2 = 1 };

enum class AdcClkmode : u8 { adc_kerclk = 0, hclkdiv2 = 1, hclkdiv4 = 2 };
enum class AdcPresc : u8 { DIV1 = 0 };

enum class AdcTrigType : u8 {
	Software = 0,
	RisingEdge = 1,
	FallingEdge = 2,
	BothEdges = 3
};

enum class AdcSampleDir : u8 { Ascending = 0, Descending = 1 };

/*
 * ADC Main Class
 */

class adc {
  public:
	constexpr adc(ADC_DEVICE iADC) : m_iADC(static_cast<u8>(iADC)) {}
	virtual void init();

	void enable();
	void disable();
	void start();
	void stop();
	void calibrate();

	adc &configureClock(AdcClkmode clockmode, AdcPresc presc = AdcPresc::DIV1);
	adc &configureTrigger(AdcTrigType trig, u8 extsel = 0);
	adc &setConversionSequence(u32 channel_bitmap,
							   AdcSampleTime smpl = AdcSampleTime::Smpl1_5,
							   AdcSampleDir dir = AdcSampleDir::Ascending);
	adc &setMode(bool cont = false, bool bulb = false);

	void waitForCompletion() const;
	bool isFinished() const;

	u16 getAdcResult() const;

	virtual void irq(u32 isr) {}

  protected:
	const u8 m_iADC;
};

/*
 * Should be configured as either continous or with external trigger to work
 * correctly!
 */
class BulkADC : public adc {
  public:
	constexpr BulkADC(ADC_DEVICE iADC) : adc(iADC) {}

	virtual void init() override;
	void irq(u32 isr) override;
	void configureBulkConversion(u16 *buffer, size_t len, bool circ = false);

  private:
	size_t m_buflen = 0;
	size_t m_idx = 0;
	u16 *m_buffer = nullptr;
	bool m_circ = false;
};

} // namespace stlib
#endif /* INC_LIB_ADC_H_ */
