/*
 * haldefs.h
 *
 *  Created on: 28.12.2018
 *      Author: alex
 */

#pragma once

#include <cstddef>
#include <cstdint>
#include <type_traits>

using u8 = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;

using s8 = int8_t;
using s16 = int16_t;
using s32 = int32_t;

template <typename t>
using isInt_t = std::enable_if_t<std::is_integral<t>::value>;

#ifdef USE_CCM
#define ccm_func __attribute__((section(".cmmtext")))
#else
#define ccm_func
#endif
