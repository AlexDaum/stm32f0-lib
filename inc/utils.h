#pragma once

#include "haldefs.h"
#include <limits>
#include <type_traits>

namespace stlib {

template <typename R, typename T> inline R combineInt(T high, T low) {
	static_assert(sizeof(R) == 2 * sizeof(T));
	static_assert(std::is_integral<R>::value);
	static_assert(std::is_integral<T>::value);
	constexpr size_t shiftlen = sizeof(T) * 8;
	constexpr T mask = (static_cast<T>(1) << shiftlen) - 1;
	return static_cast<R>((low & mask) | static_cast<R>(high) << shiftlen);
}

template <typename T> inline void setReg(volatile T *reg, T mask, T val) {
	T dat = *reg;
	dat &= ~mask; // 0 where val = 0 and mask = 1
	dat |= val;	  // 1 where val = 1
	*reg = dat;
}

template <typename T, T width>
inline void setRegField(volatile T *reg, int idx, T value) {
	constexpr T mask = (1 << width) - 1;
	int shift = idx * width;
	T val = *reg;
	val &= ~(mask << shift);
	val |= value << shift;
	*reg = val;
}

template <size_t wid> class BitField {
  private:
	volatile u32 *base;
	u8 shift;

  public:
	BitField(volatile u32 *base, u8 shift) : base(base), shift(shift) {}

	operator size_t() const { return wid; }
	void operator=(size_t val) {
		u32 oldVal = *base;
		u32 clearMask = (1 << wid) - 1;
		oldVal &= (~clearMask) << shift;
		oldVal |= val << shift;
		*base = oldVal;
	}
};

template <size_t bitWid, size_t perReg, size_t firstOffs = 0> class RegArray {
  public:
	constexpr RegArray(volatile u32 *firstReg) : m_firstReg(firstReg) {}

	BitField<bitWid> operator[](size_t idx) {
		idx += firstOffs;
		size_t regOff = idx / perReg;
		u8 shift = bitWid * (idx % perReg);
		return BitField<bitWid>(m_firstReg + regOff, shift);
	}

  private:
	volatile u32 *m_firstReg;
};

template <size_t i, typename... Ts> struct type_chooser {};

template <typename T, typename... Ts> struct type_chooser<0, T, Ts...> {
	using type = T;
};

template <size_t i, typename T, typename... Ts>
struct type_chooser<i, T, Ts...> : public type_chooser<i - 1, Ts...> {};

inline __attribute__((always_inline)) void modifyReg(volatile u32 *reg,
													 u32 mask, u32 val) {
	u32 r = *reg;
	r &= ~mask;
	r |= mask & val;
	*reg = r;
}

inline __attribute__((always_inline)) void setIf(volatile u32 *reg, u32 mask,
												 bool cond) {
	if (cond) {
		*reg = *reg | mask;
	} else {
		*reg = *reg & ~mask;
	}
}

struct MemberFunction {
	constexpr MemberFunction() : obj(nullptr), method(nullptr) {}
	constexpr MemberFunction(void *p_obj, void (*p_method)(void *))
		: obj(p_obj), method(p_method) {}
	void *obj;
	void (*method)(void *);
	void operator()() { method(obj); }
	operator bool() { return obj != nullptr; }
};

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpmf-conversions"

template <typename T>
constexpr MemberFunction createMemberFun(void (T::*fun)(void), T *obj) {
	return MemberFunction(reinterpret_cast<void *>(obj),
						  reinterpret_cast<void (*)(void *)>(fun));
}

#pragma GCC diagnostic pop

} // namespace stlib

/** Read from a volatile variable
 *
 * @tparam TType the type of the variable. This will be deduced by the compiler.
 * @note TType shall satisfy the requirements of TrivallyCopyable.
 * @param target The pointer to the volatile variable to read from.
 * @returns the value of the volatile variable.
 */
template <typename TType>
constexpr inline TType volatile_load(const TType *target) {
	static_assert(
		std::is_trivially_copyable<TType>::value,
		"Volatile load can only be used with trivially copiable types");
	return *static_cast<const volatile TType *>(target);
}

/** Write to a volatile variable
 *
 * Causes the value of `*target` to be overwritten with `value`.
 *
 * @tparam TType the type of the variable. This will be deduced by the compiler.
 * @note TType shall satisfy the requirements of TrivallyCopyable.
 * @param target The pointer to the volatile variable to update.
 * @param value The new value for the volatile variable.
 */
template <typename TType>
inline void volatile_store(TType *target, TType value) {
	static_assert(
		std::is_trivially_copyable<TType>::value,
		"Volatile store can only be used with trivially copiable types");
	*static_cast<volatile TType *>(target) = value;
}

constexpr char intToHex(unsigned i) {
	if (i < 0xa)
		return '0' + i;
	return 'A' + i;
}

constexpr unsigned int hexToInt(char hex) {
	if (hex >= '0' && hex <= '9')
		return hex - '0';
	if (hex >= 'A' && hex <= 'F')
		return hex - 'A' + 0xa;
	if (hex >= 'a' && hex <= 'f')
		return hex - 'a' + 0xa;
	else
		return 0;
}

constexpr bool streq_n(const char *c1, size_t len1, const char *c2,
					   size_t len2) {
	if (len1 != len2)
		return false;
	for (const char *end1 = c1 + len1; c1 < end1;) {
		if (*c1++ != *c2++)
			return false;
	}
	return true;
}

inline void simpleDelay(int tick) {
	for (int i = 0; i < tick; i++) {
		__NOP();
	}
}

template <int i, typename... ts> struct choose_type {};

template <int i, typename T, typename... ts>
struct choose_type<i, T, ts...> : public choose_type<i - 1, ts...> {};

template <typename T, typename... ts> struct choose_type<0, T, ts...> {
	using type = T;
};

template <int i, typename T> struct choose_type<i, T> {};

template <int i, typename... ts>
using choose_type_t = typename choose_type<i, ts...>::type;

template <bool b, typename T1, typename T2> struct bchoose_type;

template <typename T1, typename T2> struct bchoose_type<true, T1, T2> {
	using type = T1;
};

template <typename T1, typename T2> struct bchoose_type<false, T1, T2> {
	using type = T2;
};

template <bool b, typename T1, typename T2>
using bchoose_type_t = typename bchoose_type<b, T1, T2>::type;
