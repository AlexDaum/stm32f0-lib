#pragma once

#include "haldefs.h"

namespace stlib {

enum class TimerType : u8 {
	AdvancedControl = 1,
	GeneralPurpose = 2,
	Basic = 4
};

} // namespace stlib
