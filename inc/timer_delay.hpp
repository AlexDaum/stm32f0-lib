#pragma once

#include "timer.h"

namespace stlib {

class TimerDelay {
  public:
	virtual void delay(u32 i, timebase_t timebase) = 0;
};

template <typename T> class TimerDelayImpl : public TimerDelay {
  public:
	constexpr explicit TimerDelayImpl(Timer<T> &tim) : timer(tim) {}
	virtual void delay(u32 i, timebase_t timebase) override {
		timer.setPresc(SystemCoreClock / static_cast<u32>(timebase));
		timer.zeroCount();
		timer.start();
		while (timer.getCount() < i)
			;
		timer.stop();
	}

  private:
	Timer<T> &timer;
};

} // namespace stlib
