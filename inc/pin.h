#pragma once

#include <array>
#include <haldefs.h>
#include <stdint-gcc.h>
#include <stm.h>

namespace stlib {
class Port {
	friend class Pin;

  public:
	constexpr Port(int iPort) : m_iPort(iPort) {}

	u16 get() const { return getReg()->IDR; }
	void enableClock() const;

  private:
	GPIO_TypeDef *getReg() const;

	const u8 m_iPort;
};

enum class PinMode : u8 { INPUT = 0, OUTPUT = 1, ALTERNATE = 2, ANALOG = 3 };
enum class PinOType : u8 { PUSH_PULL = 0, OPEN_DRAIN = 1 };
enum class PinSpeed : u8 { LOW = 0, MEDIUM = 1, HIGH = 2, VERY_HIGH = 3 };
enum class PinPUPD : u8 { NONE = 0, PULL_UP = 1, PULL_DOWN = 2 };
enum class PinState : bool { OFF = false, ON = true };

class Pin {
  public:
	constexpr Pin(const Port &port, u8 iPin) : m_port(port), m_iPin(iPin) {}
	const Pin &set_mode(PinMode mode) const;
	const Pin &set_pulldown(PinPUPD pupd) const;
	const Pin &set_otype(PinOType type) const;
	const Pin &setSpeed(PinSpeed speed) const;
	const Pin &setAF(u8 af) const;

	void set() const { m_port.getReg()->BSRR = 1 << m_iPin; }
	void reset() const { m_port.getReg()->BRR = 1 << m_iPin; }
	void set(PinState state) const { set(static_cast<bool>(state)); }
	void set(bool state) const;
	void toggle() const { m_port.getReg()->ODR ^= 1 << m_iPin; }
	bool get() { return (m_port.get() & (1 << m_iPin)); }

	void operator=(PinState b) const { set(b); }

  private:
	const Port &m_port;
	u8 m_iPin;
};

} // namespace stlib
#include "impl/pin_impl.hpp"
