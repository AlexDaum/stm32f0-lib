#pragma once

#include "dac.h"
#include "dma.h"

namespace stlib {

class DmaDac : public dac {
  public:
	constexpr DmaDac(DAC_DEVICE iDAC, DMAChannel dmaChannel)
		: dac(iDAC), m_dmaChannel(dmaChannel) {}

	void setDmaData_ch1(u16 *buffer, u16 len);
#ifdef DAC2CH
	void setDmaData_ch2(u16 *buffer, u16 len);
#endif
  private:
	DMAChannel m_dmaChannel;
};

}; // namespace stlib
