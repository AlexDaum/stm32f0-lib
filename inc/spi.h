#pragma once

#include "callback.hpp"
#include <functional>
#include <haldefs.h>
#include <memory>
#include <stddef.h>
#include <stm.h>

namespace stlib {

enum class SPI_BAUDRATE : u8 { div2, div4, div8, div16, div32, div64, div128, div256 };

enum class SPI_DEVICE : size_t { spi1 = 0, spi2 = 1, spi3 = 2 };

class SPIBase {
  public:
	constexpr SPIBase(SPI_DEVICE iSPI) : m_iSPI(static_cast<size_t>(iSPI)) {}
	virtual void initialize();
	virtual ~SPIBase() {}
	void configSPI(SPI_BAUDRATE baud, bool master, bool cpol, bool cpha,
				   bool ssm, bool ssi, bool frxth = true);

	void enable();
	void disable();

  protected:
	const size_t m_iSPI;
};

class SPIMasterBase : public SPIBase {
  public:
	constexpr SPIMasterBase(SPI_DEVICE iSPI) : SPIBase(iSPI) {}
	void configSPI(SPI_BAUDRATE baud, bool cpol = false, bool cpha = false) {
		SPIBase::configSPI(baud, true, cpol, cpha, true, true);
	}
	u8 transfer(u8 data);
	void send(u8 data);
	u8 receive();
	u8 getData();
	void sync();
};

class SPIBlockMaster : public SPIMasterBase {
  public:
	constexpr SPIBlockMaster(SPI_DEVICE iSPI) : SPIMasterBase(iSPI) {}
	virtual ~SPIBlockMaster() {}

	virtual void sendBlock(u8 *data, size_t len) final;
	virtual void receiveBlock(u8 *data, size_t len) final;
	virtual void transferBlock(u8 *data, size_t len) final;

	virtual void sendBlockAsync(u8 *data, size_t len,
								callback_ptr callb = nullptr);
	virtual void sendBlockAsyncCopy(u8 *data, size_t len,
									callback_ptr callb = nullptr);
	virtual void receiveBlockAsync(u8 *data, size_t len,
								   callback_ptr callb = nullptr);
};

} // namespace stlib
