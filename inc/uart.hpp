#pragma once

#include "haldefs.h"
#include "stm.h"
#include "utils.h"
#include <functional>
#include <string_view>


#ifndef USART_CR1_M0
#define USART_CR1_M0 (1 << 12)
#endif

#ifndef USART_CR1_M1
#define USART_CR1_M1 (1 << 28)
#endif

namespace stlib {

void attachUartInterrupt(size_t idx, const MemberFunction &);

enum class UartDevice : u8 { Usart1 = 0, Usart2 = 1, Usart3 = 2, Uart4 = 3 };
enum class UartDataFormat : u8 { l7bit = 2, l8bit = 0, l9bit = 1 };
enum class UartParity : u8 { None, Even, Odd };
enum class UartStopBit : u8 { stop1 = 0, stop0_5 = 1, stop2 = 2, stop1_5 = 3 };

struct UartConfig {
	UartDataFormat format = UartDataFormat::l8bit;
	UartParity parity = UartParity::None;
	UartStopBit stopBits = UartStopBit::stop1;
};

class UartBase {
  public:
	virtual void send(u8) = 0;

	virtual bool dataReady() = 0;
	virtual u8 getData() = 0;

	void sendStr(std::string_view str) {
		for (char c : str) {
			send(c);
		}
	}
	void newline() {
		send('\r');
		send('\n');
	}
	void sendArr(u8 *arr, size_t n) {
		for (size_t i = 0; i < n; i++) {
			send(arr[i]);
		}
	}
	size_t getData(u8 *arr, size_t n) {
		size_t i = 0;
		while (dataReady() && i < n) {
			arr[i] = getData();
		}
		return i;
	}
	void waitForData() {
		while (!dataReady())
			;
	}

	UartBase &operator<<(std::string_view str){
		sendStr(str);
		return *this;
	}

	UartBase &operator<<(char c){
		send(c);
		return *this;
	}
};

class Uart : public UartBase {
  public:
	constexpr explicit Uart(UartDevice iUart) : m_iUart(iUart) {}
	void init();
	void setBaud(u32 targetBaud);

	void enableTransmitter() { getUart()->CR1 |= USART_CR1_TE; }
	void disableTransmitter() { getUart()->CR1 &= ~USART_CR1_TE; }
	void enableReceiver() { getUart()->CR1 |= USART_CR1_RE; }
	void disableReceiver() { getUart()->CR1 &= ~USART_CR1_RE; }
	virtual void configure(const UartConfig &cfg);

	void enable() { getUart()->CR1 |= USART_CR1_UE; }
	void disable() { getUart()->CR1 &= ~USART_CR1_UE; }

	void send(u8 d) override {
		auto uart = getUart();
		while (!(uart->ISR & USART_ISR_TXE))
			;
		uart->TDR = d;
	}

	bool dataReady() override { return getUart()->ISR & USART_ISR_RXNE; }
	bool dataReadyIgnoreOverrun() {
		bool rdy = dataReady();
		if (rdy)
			return rdy;
		auto uart = getUart();
		uart->ICR = USART_ICR_ORECF | USART_ICR_FECF;
		return false;
	}

	u8 getData() override { return getUart()->RDR; }

  protected:
	USART_TypeDef *getUart();

  protected:
	const UartDevice m_iUart;
};

} // namespace stlib
