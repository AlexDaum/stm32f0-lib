#pragma once

#include "pin.h"
#include "spi.h"
#include "haldefs.h"

namespace stlib {

class SPIDevice {
  public:
	constexpr SPIDevice(SPIBlockMaster &spi, const Pin &SSPin)
		: m_spi(spi), m_SSPin(SSPin) {}
	virtual ~SPIDevice() {
		if (m_dBuffer)
			delete m_dBuffer;
	}

  protected:
	virtual void onReceive() = 0;
	virtual void onTransmit() = 0;

	void wait_and_ss_low();
	void ss_high();

	SPIBlockMaster &m_spi;
	const Pin &m_SSPin;
	u8 *m_dBuffer = nullptr;

	volatile bool m_isActive = false;
};

} // namespace stlib

inline void stlib::SPIDevice::wait_and_ss_low() {
	while (m_isActive)
		__WFI();
	m_SSPin.reset();
}

inline void stlib::SPIDevice::ss_high() {
	m_SSPin.set();
}
