#pragma once
#include "spi_device.h"
#include "timer_delay.hpp"
namespace stlib {

struct SPIFlashJEDECID {
	u8 Manufacturer;
	u8 MemoryType;
	u8 MemoryCapacity;

	u32 toInt() {
		return (Manufacturer << 16) | (MemoryType << 8) | (MemoryCapacity);
	}
};

class SPIFlash : public SPIDevice {
  public:
	constexpr SPIFlash(SPIBlockMaster &spi, const Pin &SSPin)
		: SPIDevice(spi, SSPin) {}

	u8 readStatusReg();
	SPIFlashJEDECID readJEDECID();
	void read(u32 addr, u8 *buf, size_t len);
	void readAsnyc(u32 addr, u8 *buf, size_t len, callback_ptr callb = nullptr);
	void write(u32 addr, u8 *buf, size_t len);
	void writeAsync(u32 addr, u8 *buf, size_t len,
					callback_ptr callb = nullptr);

	void erase4k(u32 blockAddr);
	void erase32k(u32 blockAddr);
	void erase64k(u32 blockAddr);
	void eraseChip();

	void enterSleep();
	void exitSleep();

	bool isBusy();
	void wait_while_busy();

  protected:
	void writeEnable();

	virtual void onReceive() override;
	virtual void onTransmit() override;

  private:
	void programSinglePage(u32 addr, u8 *data, size_t len);
	void sendSimpleCommand(u8 command);
	void sendErase(u8 eraseCommand, u32 address);
	void send24bitAddr(u32 address);

	static constexpr size_t pageSize = 256;

	static constexpr u8 SPIFLASH_SR_BUSY = 1 << 0;

	static constexpr u8 cmd_WriteEnable = 0x06;
	static constexpr u8 cmd_ReadStatusRegister = 0x05;
	static constexpr u8 cmd_ReadJEDECID = 0x9F;
	static constexpr u8 cmd_Erase4k = 0x20;
	static constexpr u8 cmd_Erase32k = 0x52;
	static constexpr u8 cmd_Erase64k = 0xD8;
	static constexpr u8 cmd_EraseChip = 0xC7;
	static constexpr u8 cmd_FastRead = 0x0B;
	static constexpr u8 cmd_PageProgram = 0x02;
	static constexpr u8 cmd_PowerDown = 0xB9;
	static constexpr u8 cmd_ReleasePD = 0xAB;
};
} // namespace stlib
