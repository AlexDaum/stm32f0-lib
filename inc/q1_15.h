#pragma once

class q1_15 {
  private:

  public:
	constexpr q1_15(short data) : data(data) {}

	constexpr q1_15() : data(0) {}
	/*create a new q1.15 number with a value, that is scaled with a factor,
	 that is of form 2^exp.
	 The resulting number will be value/(2^exp) and must be between -1 and 1
	 */
	constexpr q1_15(short value, int exp) : data(value * (1 << (15 - exp))) {}

	static constexpr q1_15 withScale(short value, short scale) {
		long tmp = static_cast<long>(value) * static_cast<long>(scale);
		tmp >>= 16;
		return q1_15{static_cast<short>(tmp)};
	}

	constexpr q1_15 operator+(const q1_15 &o) const {
		short newData = data + o.data;
		return q1_15{newData};
	}

	constexpr q1_15 operator-(const q1_15 &o) const {
		short newData = data - o.data;
		return q1_15{newData};
	}

	constexpr q1_15 &operator+=(const q1_15 &o) {
		data += o.data;
		return *this;
	}

	constexpr q1_15 &operator-=(const q1_15 &o) {
		data -= o.data;
		return *this;
	}

	constexpr q1_15 operator*(const q1_15 &o) {
		long tmpData = static_cast<long>(data) * static_cast<long>(o.data);
		tmpData >>= 16;
		return q1_15{static_cast<short>(tmpData)};
	}

	constexpr q1_15 &operator*=(const q1_15 &o) {
		long tmpData = static_cast<long>(data) * static_cast<long>(o.data);
		tmpData >>= 16;
		data = static_cast<short>(tmpData);
		return *this;
	}

	/*
	 * Converts this q1.15 number to an integer with the scaling factor 2^exp.
	 * exp should be <= 15
	 */
	constexpr short toIntExp(int exp) { return data >> (15 - exp); }

	/*
	 * Converts this q1.15 number to an integer with a scaling factor applied
	 */
	constexpr short toInt(short scale) {
		long tmp = static_cast<long>(tmp) * static_cast<long>(scale);
		tmp >>= 15;
		return static_cast<short>(tmp);
	}

	constexpr short getRawData() { return data; }

  private:
	short data;
};
