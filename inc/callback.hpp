#pragma once

#include <memory>

namespace stlib {
class Callback {
  public:
	virtual void call() = 0;
	void operator()() { call(); }
	virtual ~Callback() {}
};
using callback_ptr = std::unique_ptr<Callback>;

template <typename other> class CombineCallback : public Callback {
  public:
	CombineCallback(callback_ptr first, other &&second)
		: m_first(std::move(first)), m_second(std::forward<other>(second)) {}

	void call() override {
		m_first->call();
		m_second();
	}

  private:
	callback_ptr m_first;
	other m_second;
};

template <typename wrapped> class CallbackWrapper : public Callback {

	explicit CallbackWrapper(wrapped &&w) : m_wrap(std::forward<wrapped>(w)) {}

	void call() override { m_wrap(); }

  private:
	wrapped m_wrap;
};

template <typename other>
callback_ptr makeCombineCallback(callback_ptr first, other &&second) {
	return std::make_unique<CombineCallback<other>>(
		std::move(first), std::forward<other>(second));
}

template <typename wrapped> callback_ptr makeCallback(wrapped &&wrap) {
	return std::make_unique<CallbackWrapper<wrapped>>(
		std::forward<wrapped>(wrap));
}

} // namespace stlib
