#pragma once

#include "haldefs.h"
#include "utils.h"

namespace stlib {

template <typename T, size_t bufSize> class Ringbuffer {
  public:
	constexpr Ringbuffer() : write(0), read(0) {}

	void put(const T &t) {
		data[write] = t;
		write = incIdx(write);
	}

	T get() {
		T t = data[read];
		read = incIdx(read);
		return t;
	}

	bool isEmpty() { return volatile_load(&write) == read; }
	bool isFull() { return (incIdx(write) == volatile_load(&read)); }

	size_t available() { return (volatile_load(&write) - read) % bufSize; }
	size_t space() { return (volatile_load(&read) - write) % bufSize; }

  private:
	size_t incIdx(size_t old) {
		size_t n = old + 1;
		if (n >= bufSize)
			n = 0;
		return n;
	}
	T data[bufSize];
	size_t write, read;
};
} // namespace stlib
