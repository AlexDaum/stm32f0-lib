#pragma once
#include "clocks.h"
#include "haldefs.h"
#include "stm.h"
#include "utils.h"

namespace stlib {

enum class CompDevice : u8 { comp1, comp2, comp3, comp4 };

enum class CompHyst : u8 { NONE = 0, LOW = 1, MEDIUM = 2, HIGH = 3 };

enum class CompPol : u8 { NonInverted = 0, Inverted = 1 };

enum class CompSpeed : u8 { HIGH = 0, MEDIUM = 1, LOW = 2, VERY_LOW = 3 };

enum class CompOut : u8 {
	NONE = 0,
	TIM1_BRK = 1,
	TIM1_IC1 = 2,
	TIM1_OCrefclear = 3,
	TIM2_IC4 = 4,
	TIM2_OCrefclear = 5,
	TIM3_IC1 = 6,
	TIM3_OCrefclear = 7
};

constexpr u8 CompINMSelVrefint = 0b011;
constexpr u8 CompINMSelVrefint_3_4 = 0b010;
constexpr u8 CompINMSelVrefint_1_2 = 0b001;
constexpr u8 CompINMSelVrefint_1_4 = 0b000;
constexpr u8 CompINMSelDACOut1 = 0b100;

class Comparator {
  public:
	constexpr explicit Comparator(CompDevice dev)
		: m_iComp(static_cast<u8>(dev)) {}
	void init() { sysCfgEn.set(); }
	void config(u8 inm, CompOut outsel = CompOut::NONE,
				CompPol pol = CompPol::NonInverted,
				CompSpeed speed = CompSpeed::HIGH,
				CompHyst hyst = CompHyst::NONE) {
		auto comp = getComp();
		const int offs = 16 * (m_iComp);
		u32 csr = 0;
		csr |= static_cast<u32>(outsel) << (COMP_CSR_COMP1OUTSEL_Pos);
		csr |= static_cast<u32>(inm) << (COMP_CSR_COMP1INSEL_Pos);
		csr |= static_cast<u32>(pol) << (COMP_CSR_COMP1POL_Pos);
		csr |= static_cast<u32>(hyst) << (COMP_CSR_COMP1HYST_Pos);
		csr |= static_cast<u32>(speed) << (COMP_CSR_COMP1MODE_Pos);
		comp->CSR = csr << offs;
	}

	void setEnabled(bool enabled) {
		u32 csr = getComp()->CSR;
		csr &= ~COMP_CSR_COMP1EN_Msk << 16 * m_iComp;
		csr |= (enabled ? 1 : 0) << (COMP_CSR_COMP1EN_Pos + 16 * m_iComp);
		getComp()->CSR = csr;
	}

  protected:
	COMP_Common_TypeDef *getComp();

  private:
	u8 m_iComp;
};
} // namespace stlib
