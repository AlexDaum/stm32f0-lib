#pragma once

#include "clocks.h"
#include "stm.h"
#include <array>

namespace stlib {

constexpr std::array<SPI_TypeDef *, 3> spi_map{SPI1, SPI2};

constexpr std::array<const clockEnableBit, 2> spi_clocks{
	clockEnableBit(CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_SPI1EN),
	clockEnableBit(CLOCK_REG_BASE(APB1ENR), RCC_APB1ENR_SPI2EN)};

} // namespace stlib
