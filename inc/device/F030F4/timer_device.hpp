#pragma once
#include "clocks.h"
#include "stm.h"
#include "timer_traits.h"
#include <array>

namespace stlib {

constexpr size_t timer_count = 5;

class TimerStaticHelper {
	template <typename T> friend class Timer;

  private:
	static constexpr std::array<TIM_TypeDef *, timer_count> timer_map{
		TIM1, TIM3, TIM14, TIM16, TIM17};
	static constexpr std::array<clockEnableBit, timer_count> timer_clocks{
		clockEnableBit{CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_TIM1EN},
		clockEnableBit{CLOCK_REG_BASE(APB1ENR), RCC_APB1ENR_TIM3EN},
		clockEnableBit{CLOCK_REG_BASE(APB1ENR), RCC_APB1ENR_TIM14EN},
		clockEnableBit{CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_TIM16EN},
		clockEnableBit{CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_TIM17EN}};
};

enum class TimerDevice : u8 {
	Timer1 = 0,
	Timer3 = 1,
	Timer14 = 2,
	Timer16 = 3,
	Timer17 = 4
};

template <TimerDevice dev> struct timer_traits {
	static constexpr TimerType type = TimerType::Basic;
	static constexpr u8 ccChannels = 0;
	static constexpr bool upDownControl = false;
	using timerDataType = u16;
};

template <> struct timer_traits<TimerDevice::Timer1> {
	static constexpr TimerType type = TimerType::AdvancedControl;
	static constexpr u8 ccChannels = 4;
	static constexpr bool upDownControl = true;
	using timerDataType = u16;
};

template <> struct timer_traits<TimerDevice::Timer3> {
	static constexpr TimerType type = TimerType::GeneralPurpose;
	static constexpr u8 ccChannels = 4;
	static constexpr bool upDownControl = true;
	using timerDataType = u16;
};

template <> struct timer_traits<TimerDevice::Timer14> {
	static constexpr TimerType type = TimerType::GeneralPurpose;
	static constexpr u8 ccChannels = 2;
	static constexpr bool upDownControl = false;
	using timerDataType = u16;
};

template <> struct timer_traits<TimerDevice::Timer16> {
	static constexpr TimerType type = TimerType::GeneralPurpose;
	static constexpr u8 ccChannels = 1;
	static constexpr bool upDownControl = false;
	using timerDataType = u16;
};

template <> struct timer_traits<TimerDevice::Timer17> {
	static constexpr TimerType type = TimerType::GeneralPurpose;
	static constexpr u8 ccChannels = 1;
	static constexpr bool upDownControl = false;
	using timerDataType = u16;
};

} // namespace stlib
