#pragma once

#include "clocks.h"
#include "stm.h"
#include <array>

namespace stlib {

constexpr std::array<SPI_TypeDef *, 1> spi_map{SPI1};

constexpr std::array<const clockEnableBit, 1> spi_clocks{
	clockEnableBit(CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_SPI1EN)};

} // namespace stlib
