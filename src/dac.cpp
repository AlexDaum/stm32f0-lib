/*
 * dac.cpp
 *
 *  Created on: 02.04.2020
 *      Author: alex
 */
#include "dac.h"
#include "clocks.h"
#include "utils.h"
#include <array>

using namespace stlib;

static constexpr std::array<clockEnableBit, 1> dac_clocks{
	clockEnableBit(CLOCK_REG_BASE(APB1ENR), RCC_APB1ENR_DACEN)};

static constexpr std::array<DAC_TypeDef *, 1> dac_map{DAC1};

void dac::init() { dac_clocks[m_iDAC].set(); }

void dac::enable_channel1() {
	auto dac = dac_map[m_iDAC];
	dac->CR |= DAC_CR_EN1;
}

void dac::set_ch1_data(u16 data) {
	auto dac = dac_map[m_iDAC];
	dac->DHR12R1 = data;
}

void stlib::dac::enable_dma_ch1() {
	auto dac = dac_map[m_iDAC];
	dac->CR |= DAC_CR_DMAEN1;
}

void stlib::dac::select_trigger_ch1(u16 trigger) {
	auto dac = dac_map[m_iDAC];
	u32 cr = dac->CR;
	cr &= ~(DAC_CR_TSEL1 | DAC_CR_TEN1);
	if (trigger != 0) {
		cr |= trigger << DAC_CR_TSEL1_Pos;
		cr |= DAC_CR_TEN1;
	}
	dac->CR = cr;
}

#ifdef DAC2CH
void dac::enable_channel2() {
	auto dac = dac_map[m_iDAC];
	dac->CR |= DAC_CR_EN2;
}
void dac::set_ch2_data(u16 data) {
	auto dac = dac_map[m_iDAC];
	dac->DHR12R2 = data;
}
void stlib::dac::enable_dma_ch2() {
	auto dac = dac_map[m_iDAC];
	dac->CR |= DAC_CR_DMAEN2;
}
void stlib::dac::select_trigger_ch2(u16 trigger) {
	auto dac = dac_map[m_iDAC];
	u32 cr = dac->CR;
	cr &= ~(DAC_CR_TSEL2 | DAC_CR_TEN2);
	if (trigger != 0) {
		cr |= trigger << DAC_CR_TSEL2_Pos;
		cr |= DAC_CR_TEN2;
	}
	dac->CR = cr;
}
void stlib::dac::set_ch2_mode(DAC_MODE mode) {
	auto dac = getDAC();
	if (mode == DAC_MODE::BufferPin) {
		dac->CR &= ~DAC_CR_BOFF2;
	} else {
		dac->CR |= DAC_CR_BOFF2;
	}
}
#endif

DAC_TypeDef *stlib::dac::getDAC() const { return dac_map[m_iDAC]; }

void stlib::dac::set_ch1_mode(DAC_MODE mode) {
	auto dac = getDAC();
	if (mode == DAC_MODE::BufferPin) {
		dac->CR &= ~DAC_CR_BOFF1;
	} else {
		dac->CR |= DAC_CR_BOFF1;
	}
}
