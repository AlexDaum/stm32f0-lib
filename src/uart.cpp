/*
 * uart.cpp
 *
 *  Created on: 13.04.2020
 *      Author: alex
 */

#include "uart.hpp"
#include "clocks.h"
#include "utils.h"
#include <array>
using namespace stlib;

constexpr size_t numUart = 2;

constexpr std::array<USART_TypeDef *, numUart> uart_map{USART1, USART2};

constexpr std::array<clockEnableBit, numUart> uart_clocks{
	clockEnableBit(CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_USART1EN),
	clockEnableBit(CLOCK_REG_BASE(APB1ENR), RCC_APB1ENR_USART2EN)
};

constexpr IRQn_Type uart_interrupts[numUart]{USART1_IRQn, USART2_IRQn};

std::array<MemberFunction, numUart> uart_irqs;

void stlib::Uart::init() { uart_clocks[static_cast<u8>(m_iUart)].set(); }

void stlib::Uart::setBaud(u32 targetBaud) {
	u32 brr = SystemCoreClock / targetBaud;
	getUart()->BRR = brr;
}

void stlib::Uart::configure(const UartConfig &cfg) {
	auto uart = getUart();
	u32 cr1 = 0;

	if (cfg.format == UartDataFormat::l7bit)
		cr1 |= USART_CR1_M1;
	else if (cfg.format == UartDataFormat::l9bit)
		cr1 |= USART_CR1_M0;

	if (cfg.parity != UartParity::None) {
		cr1 |= USART_CR1_PCE;
		if (cfg.parity == UartParity::Odd)
			cr1 |= USART_CR1_PS;
	}
	uart->CR1 = cr1;
	u32 cr2 = 0;
	cr2 |= static_cast<u32>(cfg.stopBits) << USART_CR2_STOP_Pos;
	uart->CR2 = cr2;
}

USART_TypeDef *stlib::Uart::getUart() {
	return uart_map[static_cast<u8>(m_iUart)];
}

void stlib::attachUartInterrupt(size_t idx, const MemberFunction &callb) {
	uart_irqs[idx] = callb;
	NVIC_EnableIRQ(uart_interrupts[idx]);
}

extern "C" void USART1_IRQHandler() {
	auto &cb = uart_irqs[0];
	if (cb)
		cb();
}
extern "C" void USART2_IRQHandler() {
	auto &cb = uart_irqs[1];
	if (cb)
		cb();
}
