#include "device/spi_device.hpp"
#include <array>
#include <clocks.h>
#include <spi.h>

using stlib::spi_clocks;
using stlib::spi_map;

static inline u8 readByte(volatile u32 &reg) {
	return reinterpret_cast<volatile u8 &>(reg);
}

static inline void writeByte(volatile u32 &reg, u8 val) {
	reinterpret_cast<volatile u8 &>(reg) = val;
}

static inline u16 readHalfword(volatile u32 &reg) {
	return reinterpret_cast<volatile u16 &>(reg);
}

static inline void writeHalfword(volatile u32 &reg, u16 val) {
	reinterpret_cast<volatile u16 &>(reg) = val;
}
namespace stlib {

void SPIBase::initialize() { spi_clocks[m_iSPI].set(); }

void SPIBase::configSPI(SPI_BAUDRATE baud, bool master, bool cpol, bool cpha,
						bool ssm, bool ssi, bool frxth) {
	auto spi = spi_map[m_iSPI];
	u16 cr1 = 0;
	cr1 |= static_cast<u16>(baud) << SPI_CR1_BR_Pos;
	cr1 |= static_cast<u16>(master) << SPI_CR1_MSTR_Pos;
	cr1 |= static_cast<u16>(cpol) << SPI_CR1_CPOL_Pos;
	cr1 |= static_cast<u16>(cpha) << SPI_CR1_CPHA_Pos;
	cr1 |= static_cast<u16>(ssm) << SPI_CR1_SSM_Pos;
	cr1 |= static_cast<u16>(ssi) << SPI_CR1_SSI_Pos;
	spi->CR1 = cr1;
	u16 cr2 = 0;
	cr2 |= static_cast<u16>(frxth) << SPI_CR2_FRXTH_Pos;
	spi->CR2 = cr2;
}

u8 SPIMasterBase::transfer(u8 data) {
	auto spi = spi_map[m_iSPI];
	sync();
	// send and receive
	writeByte(spi->DR, data);
	while (!(spi->SR & SPI_SR_RXNE))
		;
	return readByte(spi->DR);
}

void SPIMasterBase::send(u8 data) {
	auto spi = spi_map[m_iSPI];
	while (!(spi->SR & SPI_SR_TXE))
		;
	writeByte(spi->DR, data);
}

u8 SPIMasterBase::receive() {
	send(0);
	return getData();
}
u8 SPIMasterBase::getData() {
	auto spi = spi_map[m_iSPI];
	while (!(spi->SR & SPI_SR_RXNE))
		;
	return spi->DR;
}

void SPIBase::enable() { spi_map[m_iSPI]->CR1 |= SPI_CR1_SPE; }

void SPIBase::disable() { spi_map[m_iSPI]->CR1 &= ~SPI_CR1_SPE; }

void SPIBlockMaster::sendBlock(u8 *data, size_t len) {
	for (size_t i = 0; i < len; i++)
		send(data[i]);
}

void SPIBlockMaster::receiveBlock(u8 *data, size_t len) {
	send(0);
	for (size_t i = 0; i < len - 1; i++)
		data[i] = receive();
	while (!(spi_map[m_iSPI]->SR & SPI_SR_RXNE))
		;
	data[len - 1] = spi_map[m_iSPI]->DR;
}

void SPIBlockMaster::transferBlock(u8 *data, size_t len) {
	for (size_t i = 0; i < len; i++) {
		data[i] = transfer(data[i]);
	}
}

void SPIMasterBase::sync() {
	auto spi = spi_map[m_iSPI];
	while (spi->SR & SPI_SR_BSY)
		;
	// read receive buffer
	while (spi->SR & SPI_SR_RXNE)
		__unused u16 d = spi->DR;
}

void SPIBlockMaster::sendBlockAsync(u8 *data, size_t len, callback_ptr callb) {
	sendBlock(data, len);
	callb->call();
}

void SPIBlockMaster::sendBlockAsyncCopy(u8 *data, size_t len,
										callback_ptr callb) {
	sendBlock(data, len);
	callb->call();
}

void SPIBlockMaster::receiveBlockAsync(u8 *data, size_t len,
									   callback_ptr callb) {
	receiveBlock(data, len);
	callb->call();
}

} // namespace stlib
