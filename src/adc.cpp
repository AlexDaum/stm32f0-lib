/*
 * adc.cpp
 *
 *  Created on: 31.03.2020
 *      Author: alex
 */

#include "adc.h"
#include "clocks.h"
#include "utils.h"
#include <array>
#include <functional>

using namespace stlib;

/*
 * Helpers
 */

static constexpr std::array<clockEnableBit, 1> adcClocks{
	clockEnableBit(CLOCK_REG_BASE(APB2ENR), RCC_APB2ENR_ADC1EN)};

static constexpr std::array<ADC_TypeDef *, 1> adc_map{ADC1};

static std::array<adc *, 1> adc_with_irq{nullptr};

static constexpr u32 handled_interrupts = ADC_ISR_EOC;

/*
 * Main Code
 */

void stlib::adc::init() { adcClocks[m_iADC].set(); }

void stlib::adc::enable() {
	auto adc = adc_map[m_iADC];
	adc->CR |= ADC_CR_ADEN;
	while (!(adc->ISR & ADC_ISR_ADRDY))
		;
	adc->ISR = ADC_ISR_ADRDY;
}

void stlib::adc::disable() { adc_map[m_iADC]->CR |= ADC_CR_ADDIS; }

void stlib::adc::start() { adc_map[m_iADC]->CR |= ADC_CR_ADSTART; }

void stlib::adc::stop() { adc_map[m_iADC]->CR |= ADC_CR_ADSTP; }

void stlib::adc::calibrate() {
	auto adc = adc_map[m_iADC];
	adc->CR |= ADC_CR_ADCAL;
	while (adc->CR & ADC_CR_ADCAL)
		;
}

adc &stlib::adc::configureTrigger(AdcTrigType trig, u8 extsel) {
	auto adc = adc_map[m_iADC];
	u32 cfgr = adc->CFGR1;
	cfgr &= ~(ADC_CFGR1_EXTEN | ADC_CFGR1_EXTSEL);
	cfgr |= static_cast<u32>(trig) << ADC_CFGR1_EXTEN_Pos;
	cfgr |= extsel << ADC_CFGR1_EXTSEL_Pos;
	adc->CFGR1 = cfgr;
	return *this;
}

adc &stlib::adc::setConversionSequence(u32 channel_bitmap, AdcSampleTime smpl,
									   AdcSampleDir dir) {
	ADC_TypeDef *adc = adc_map[m_iADC];
	adc->CHSELR = channel_bitmap;
	u32 cfgr1 = adc->CFGR1;
	cfgr1 &= ~ADC_CFGR1_SCANDIR;
	cfgr1 |= static_cast<u32>(dir) << ADC_CFGR1_SCANDIR_Pos;
	adc->CFGR1 = cfgr1;
	return *this;
}

adc &stlib::adc::configureClock(AdcClkmode clockmode, AdcPresc presc) {
	auto adc = adc_map[m_iADC];
	u32 cfgr2 = adc->CFGR2;
	cfgr2 &= ~(ADC_CFGR2_CKMODE);
	cfgr2 |= static_cast<u32>(clockmode) << ADC_CFGR2_CKMODE_Pos;
	adc->CFGR2 = cfgr2;
	return *this;
}

adc &stlib::adc::setMode(bool cont, bool bulb) {
	auto adc = adc_map[m_iADC];
	u32 cfgr = adc->CFGR1;
	if (cont)
		cfgr |= ADC_CFGR1_CONT;
	else
		cfgr &= ~ADC_CFGR1_CONT;
	adc->CFGR1 = cfgr;
	u32 cfgr2 = adc->CFGR2;
	adc->CFGR2 = cfgr2;
	return *this;
}

void stlib::adc::waitForCompletion() const {
	while (!isFinished())
		;
}

bool stlib::adc::isFinished() const {
	auto adc = adc_map[m_iADC];
	return !(adc->CR & ADC_CR_ADSTART);
}

u16 stlib::adc::getAdcResult() const {
	auto adc = adc_map[m_iADC];
	return adc->DR;
}

void stlib::BulkADC::init() {
	adc::init();
	auto adc = adc_map[m_iADC];
	adc->IER |= ADC_IER_EOCIE;
	adc_with_irq[m_iADC] = this;
	NVIC_EnableIRQ(ADC1_IRQn);
}

void ccm_func stlib::BulkADC::irq(u32 isr) {
	auto adc = adc_map[m_iADC];
	if (m_buffer == nullptr) {
		stop();
		adc->ISR = handled_interrupts;
		return;
	}
	if (isr & ADC_ISR_EOC) {
		m_buffer[m_idx] = adc->DR;
		m_idx++;
		if (m_idx == m_buflen) {
			if (!m_circ) {
				stop();
				m_buffer = nullptr;
				m_idx = 0;
				m_buflen = 0;
			}
		}
	}
}

void stlib::BulkADC::configureBulkConversion(u16 *buffer, size_t len,
											 bool circ) {
	m_circ = circ;
	m_buffer = buffer;
	m_buflen = len;
}

extern "C" ccm_func void ADC1_COMP_IRQHandler() {
	u32 isr = ADC1->ISR;
	if (isr & handled_interrupts) {
		auto adc_ptr = adc_with_irq[0];
		if (adc_ptr)
			adc_ptr->irq(isr);
		else
			ADC1->ISR = isr & handled_interrupts;
	}
}
