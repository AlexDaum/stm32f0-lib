#include "systick.h"
#include "haldefs.h"
#include "stm.h"

stlib::Systick stlib::Systick::stick;

void stlib::Systick::onClockChanged(u32 clockSpeed) {
	SysTick_Config(clockSpeed / 1000);
	if (SysTick_IRQn > 0) // IRQs with < 0 are always active
		NVIC_EnableIRQ(SysTick_IRQn);
}

ccm_func void stlib::SysTimer::tick() { time = time + 1; }

void stlib::SysTimer::wait(uint32_t delay) {
	this->time = 0;
	while (this->time < delay)
		__WFI();
}

void stlib::Systick::irq() {
	root->forEachAfter<Ticker>(
		[](auto ptr) { static_cast<Ticker *>(ptr)->tick(); });
}

extern "C" ccm_func void SysTick_Handler() { stlib::Systick::getST().irq(); }

void stlib::SysCounter::tick() { time = time + 1; }

void stlib::SysCounter::reset() { time = 0; }

u32 stlib::SysCounter::get() { return time; }
