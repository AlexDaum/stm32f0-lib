/*
 * spi_device.cpp
 *
 *  Created on: 14.03.2020
 *      Author: alex
 */

#include "timebase.h"
#include <algorithm>
#include <haldefs.h>
#include <pin.h>
#include <spi.h>
#include <spi_flash.h>
#include <stddef.h>
#include <sys/cdefs.h>

u8 stlib::SPIFlash::readStatusReg() {
	wait_and_ss_low();
	m_isActive = true;
	m_spi.send(cmd_ReadStatusRegister);
	__unused u8 dummy = m_spi.receive();
	u8 d = m_spi.getData();
	ss_high();
	m_isActive = false;
	return d;
}

void stlib::SPIFlash::onReceive() { ss_high(); }

void stlib::SPIFlash::read(u32 addr, u8 *buf, size_t len) {
	wait_and_ss_low();
	m_isActive = true;
	m_spi.send(cmd_FastRead);
	send24bitAddr(addr);
	m_spi.send(0); // dummy
	m_spi.sync();
	m_spi.receiveBlock(buf, len);
	ss_high();
	m_isActive = false;
}

void stlib::SPIFlash::write(u32 addr, u8 *buf, size_t len) {
	// Flash is organized in pages and only a single page can be programmed at a
	// time

	u32 pageAddr = addr % pageSize;
	u32 writeSize = std::min<u32>(len, pageSize - pageAddr);
	while (len) {
		programSinglePage(addr, buf, writeSize);
		addr += writeSize;
		buf += writeSize;
		len -= writeSize;
		writeSize = std::min<u32>(len, pageSize);
	}
}

void stlib::SPIFlash::programSinglePage(u32 addr, u8 *buf, size_t len) {
	wait_while_busy();
	writeEnable();
	wait_and_ss_low();
	m_spi.send(cmd_PageProgram);
	send24bitAddr(addr);
	m_spi.sendBlock(buf, len);
	m_spi.sync();
	ss_high();
}

void stlib::SPIFlash::erase4k(u32 blockAddr) {
	sendErase(cmd_Erase4k, blockAddr);
}

void stlib::SPIFlash::erase32k(u32 blockAddr) {
	sendErase(cmd_Erase32k, blockAddr);
}

void stlib::SPIFlash::erase64k(u32 blockAddr) {
	sendErase(cmd_Erase64k, blockAddr);
}

void stlib::SPIFlash::eraseChip() {
	writeEnable();
	sendSimpleCommand(cmd_EraseChip);
}

bool stlib::SPIFlash::isBusy() { return readStatusReg() & SPIFLASH_SR_BUSY; }

void stlib::SPIFlash::wait_while_busy() {
	while (isBusy()) {
		__NOP();
	}
}

void stlib::SPIFlash::writeEnable() { sendSimpleCommand(cmd_WriteEnable); }

void stlib::SPIFlash::onTransmit() {
	m_spi.sync();
	m_isActive = false;
	m_SSPin.set();
}

stlib::SPIFlashJEDECID stlib::SPIFlash::readJEDECID() {
	while (m_isActive)
		;
	m_SSPin.reset();
	m_spi.sync();
	m_spi.send(cmd_ReadJEDECID);
	SPIFlashJEDECID id;
	__unused u8 dummy = m_spi.receive();
	id.Manufacturer = m_spi.receive();
	id.MemoryType = m_spi.receive();
	id.MemoryCapacity = m_spi.getData();
	m_SSPin.set();
	return id;
}

void stlib::SPIFlash::sendSimpleCommand(u8 command) {
	wait_and_ss_low();
	m_spi.send(command);
	m_spi.sync();
	ss_high();
}

void stlib::SPIFlash::sendErase(u8 eraseCommand, u32 address) {
	writeEnable();
	wait_and_ss_low();
	m_spi.send(eraseCommand);
	send24bitAddr(address);
	m_spi.sync();
	ss_high();
}

void stlib::SPIFlash::send24bitAddr(u32 address) {
	m_spi.send((address >> 16) & 0xFF);
	m_spi.send((address >> 8) & 0xFF);
	m_spi.send((address)&0xFF);
}

void stlib::SPIFlash::readAsnyc(u32 addr, u8 *buf, size_t len,
								callback_ptr callb) {
	wait_and_ss_low();
	m_spi.send(cmd_FastRead);
	send24bitAddr(addr);
	m_spi.send(0); // dummy
	m_spi.sync();
	m_spi.receiveBlockAsync(
		buf, len,
		makeCombineCallback(std::move(callb), [this]() { onReceive(); }));
}

void stlib::SPIFlash::writeAsync(u32 addr, u8 *buf, size_t len,
								 callback_ptr callb) {
	writeEnable();
	wait_and_ss_low();
	m_spi.send(cmd_PageProgram);
	send24bitAddr(addr);
	m_spi.sendBlockAsync(
		buf, len,
		makeCombineCallback(std::move(callb), [this]() { onTransmit(); }));
}

void stlib::SPIFlash::enterSleep() { sendSimpleCommand(cmd_PowerDown); }

void stlib::SPIFlash::exitSleep() { sendSimpleCommand(cmd_ReleasePD); }
