/*
 * timer.cpp
 *
 *  Created on: 03.04.2020
 *      Author: alex
 */

#include "timer.h"

using namespace stlib;
template <> void Timer<u16>::setApproxFreq(u32 targetFreq) {
	auto tim = getTim();
	u32 clockFactor = SystemCoreClock / targetFreq;
	u32 exp = 31 - __builtin_clzl(
					   clockFactor); // the smallest exponent, so that for
									 // clockFactor = m*2^exp -> 0 < m < 2^16
	if (exp >= 16) {
		clockFactor >>= exp - 15;
		tim->PSC = (1 << (exp - 15)) - 1; // prescaler is PSC + 1
	}
	tim->ARR = clockFactor;
}

template <> void Timer<u32>::setApproxFreq(u32 targetFreq) {
	auto tim = getTim();
	u32 clockFactor = SystemCoreClock / targetFreq;
	tim->ARR = clockFactor;
}

template <>
void Timer<u16>::setApproxPeriod(u32 period, timebase_t timebase) {
	auto tim = getTim();
	u32 clockFactor = period * (SystemCoreClock / static_cast<u32>(timebase));
	u32 exp =
		31 - __builtin_clzl(clockFactor); // the position of the highest set bit
	if (exp >= 16) {
		clockFactor >>= exp - 15;
		tim->PSC = (1 << (exp - 15)) - 1; // prescaler is PSC + 1
	}
	tim->ARR = clockFactor;
}

template <>
void Timer<u32>::setApproxPeriod(u32 period, timebase_t timebase) {
	auto tim = getTim();
	uint64_t clockFactor =
		static_cast<uint64_t>(period) *
		static_cast<uint64_t>(SystemCoreClock / static_cast<u32>(timebase));

	u32 exp = 63 - __builtin_clzll(clockFactor);
	if (exp >= 32) {
		clockFactor >>= exp - 31;
		tim->PSC = (1 << std::min<u32>(exp - 31, 16)) - 1;
	}
	tim->ARR = static_cast<u32>(clockFactor);
}
