/*
 * dma-dac.cpp
 *
 *  Created on: 03.04.2020
 *      Author: alex
 */

#include "dma-dac.h"
#include <array>

using namespace stlib;

void stlib::DmaDac::setDmaData_ch1(u16 *buffer, u16 len) {
	DMAChannelConfig dmaConf;
	dmaConf.setCirc(true)
		.setDir(DMADir::MemToPeriph)
		.setMSize(DMAMemSize::size16bit)
		.setPSize(DMAMemSize::size32bit)
		.setMinc(true);
	m_dmaChannel.configure(dmaConf);
	m_dmaChannel.configureTransfer(
		reinterpret_cast<uintptr_t>(buffer),
		reinterpret_cast<uintptr_t>(&getDAC()->DHR12R1), len);
	// TODO getDac Channel
	//	DMAMux::configureChannel(m_dmaChannel.getMuxChannelNum(),
	//							 dmamux_map_ch1[getiDAC()]);
	m_dmaChannel.enable();
}

#ifdef DAC2CH

void stlib::DmaDac::setDmaData_ch2(u16 *buffer, u16 len) {
	DMAChannelConfig dmaConf;
	dmaConf.setCirc(true)
		.setDir(DMADir::MemToPeriph)
		.setMSize(DMAMemSize::size16bit)
		.setPSize(DMAMemSize::size32bit)
		.setMinc(true);
	m_dmaChannel.configure(dmaConf);
	//	m_dmaChannel.configureTransfer(
	//		reinterpret_cast<uintptr_t>(buffer),
	//		reinterpret_cast<uintptr_t>(&getDAC()->DHR12R2), len);
	//	DMAMux::configureChannel(m_dmaChannel.getMuxChannelNum(),
	//							 dmamux_map_ch2[getiDAC()]);
	m_dmaChannel.enable();
}
#endif
