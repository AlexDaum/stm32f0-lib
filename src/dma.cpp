/*
 * dma.cpp
 *
 *  Created on: 02.04.2020
 *      Author: alex
 */

#include "dma.h"
#include "clocks.h"
#include <array>

using namespace stlib;

static constexpr u32 DMAChannelDifference = 20;

static constexpr std::array<clockEnableBit, 1> dma_clocks{
	clockEnableBit(CLOCK_REG_BASE(AHBENR), RCC_AHBENR_DMAEN)};

static constexpr std::array<DMA_TypeDef *, 1> dma_map{DMA1};

static constexpr DMA_Channel_TypeDef *getDmaChannel(u8 iDMA, u8 iChannel) {
	if (iDMA == 0) {
		return reinterpret_cast<DMA_Channel_TypeDef *>(
			DMA1_Channel1_BASE + DMAChannelDifference * (iChannel - 1));
	} else
		return DMA1_Channel1;
}

void stlib::DMA::init() { dma_clocks[m_iDMA].set(); }

void stlib::DMAChannel::configure(DMAChannelConfig config) {
	auto channel = getDmaChannel(m_iDMA, m_iChannel);
	u32 ccr = channel->CCR;
	ccr &= ~(DMA_CCR_CIRC | DMA_CCR_DIR | DMA_CCR_MEM2MEM | DMA_CCR_MINC |
			 DMA_CCR_MSIZE | DMA_CCR_PINC | DMA_CCR_PSIZE);
	ccr |= config.config;
	channel->CCR = ccr;
}

void stlib::DMAChannel::configureTransfer(uintptr_t mAddr, uintptr_t pAddr,
										  size_t count) {
	auto channel = getDmaChannel(m_iDMA, m_iChannel);
	channel->CMAR = mAddr;
	channel->CPAR = pAddr;
	channel->CNDTR = count;
}

void stlib::DMAChannel::enable() {
	auto channel = getDmaChannel(m_iDMA, m_iChannel);
	channel->CCR |= DMA_CCR_EN;
}

void stlib::DMAChannel::disable() {
	auto channel = getDmaChannel(m_iDMA, m_iChannel);
	channel->CCR &= ~DMA_CCR_EN;
}
