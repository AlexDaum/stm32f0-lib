/*
 * clock_config.cpp
 *
 *  Created on: May 14, 2020
 *      Author: alex
 */

#include "clocks.h"
#include "stm.h"

using namespace stlib::clock;

ClockChangeHandler ClockChangeHandler::instance;

void stlib::clock::enableHSI() {
	RCC->CR |= RCC_CR_HSION;
	while (!(RCC->CR & RCC_CR_HSIRDY))
		;
}
void stlib::clock::enableHSE() {
	RCC->CR |= RCC_CR_HSEON;
	while (!(RCC->CR & RCC_CR_HSERDY))
		;
}

void stlib::clock::clockConfig::switchClock(u32 sw) {
	MODIFY_REG(RCC->CFGR, RCC_CFGR_SW, sw);
	u32 cfgr;
	do {
		cfgr = RCC->CFGR;
	}
#if RCC_CFGR_SWS_Pos > RCC_CFGR_SW_Pos
#define shift (RCC_CFGR_SWS_Pos - RCC_CFGR_SW_Pos)
	while ((cfgr & RCC_CFGR_SWS) != (sw << shift));
#else
#define shift (RCC_CFGR_SW_Pos - RCC_CFGR_SWS_Pos)
	while ((cfgr & RCC_CFGR_SWS) != (sw >> shift));
#endif
}

void stlib::clock::clockConfig::confPLL() const {
	// disable PLL
	// switch to HSI clock
	switchClock(RCC_CFGR_SW_HSI);
	disablePLL();
	// enable the clock needed for the pll
	switch (m_pllsrc) {
	case PLLSource::HSEprediv:
		enableHSE();
		break;
	case PLLSource::HSIdiv2:
	case PLLSource::HSIprediv:
	default:
		enableHSI();
		break;
	}
	// wait for PLL to shut down
	while (RCC->CR & RCC_CR_PLLRDY)
		;
	// config the pll and bus prescalers
	uint32_t cfgr = RCC->CFGR;
	cfgr &= ~RCC_CFGR_PLLMUL & ~RCC_CFGR_PLLSRC;
	cfgr |= static_cast<u32>(m_pll_mul - 2)
			<< RCC_CFGR_PLLMUL_Pos; // PLLMUL starts at 2
	cfgr |= static_cast<u32>(m_pllsrc) << RCC_CFGR_PLLSRC_Pos;

	cfgr &= ~RCC_CFGR_PPRE & ~RCC_CFGR_HPRE;
	cfgr |= static_cast<u32>(m_ppre) << RCC_CFGR_PPRE_Pos;
	cfgr |= static_cast<u32>(m_hpre) << RCC_CFGR_HPRE_Pos;
	RCC->CFGR = cfgr;

	if (m_pllsrc != PLLSource::HSIdiv2) {
		u32 cfgr2 = RCC->CFGR2;
		cfgr2 &= ~RCC_CFGR2_PREDIV;
		cfgr2 |= (m_pll_presc - 1)
				 << RCC_CFGR2_PREDIV_Pos; // prediv starts at 1
		RCC->CFGR2 = cfgr2;
	}

	// enable the pll
	RCC->CR |= RCC_CR_PLLON;
	// wait for PLL to start
	while (!(RCC->CR & RCC_CR_PLLRDY))
		;
}
