/*
 * comparator.cpp
 *
 *  Created on: 10.04.2020
 *      Author: alex
 */

#include "comparator.hpp"
#include <array>

static constexpr size_t numComp = 2;

static constexpr COMP_Common_TypeDef *comp_map[numComp]{COMP12_COMMON,
														COMP12_COMMON};

COMP_Common_TypeDef *stlib::Comparator::getComp() { return comp_map[m_iComp]; }
